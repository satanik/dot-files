FROM alpine:latest

RUN apk update \
    && apk upgrade \
    && apk add --no-cache bash ncurses tmux vim expect perl bash-completion

RUN sed -i -e "s/bin\/ash/bin\/bash/" /etc/passwd
ENV SHELL /bin/bash

COPY . /opt/dot-files

CMD tail -f /dev/null