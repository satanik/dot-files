# dot files

> **NOTICE: This README.md is still work in progress and does not contain enough information to utilise this repository fully**

This is a collection of `dot-files` for the author's convenience. The content of those files was tailored carefully year after year to make it easier to bootstrap a comfortable environment in a new system.

The `dot-files` are composed of a variety of files and directories from different use-cases, i.e. for different tools.

To install these, refer to the [dot files CLI](https://gitlab.com/satanik/dot-files-cli).

## 1. Components

The structure tries to separate by responsibility and to keep it as configurable as possible. Thus there are many files for many different settings included.

```console
+ candidates
  + bash/
    + .profiles/
      - .composer_profile
      - .go_profile
      - .imagemagick_profile
      - .less_profile
      - .openssl_profile
      - .pyenv_profile
      - .python_profile
      - .rbenv_profile
      - .rvm_profile
      - .thefuck_profile
      - .vagrant_profile
      - .vim_profile
      - .virtualenvwrapper_profile
    - .bash_aliases
    - .bash_functions
    - .bash_profile
    - .bash_prompt
  + bin/
    + .bin/
      - delete-gitlab-release*
      - error*
      - extract*
      - lean*
      - msg*
      - parse_yaml*
      - prename*
      - safe_rm*
      - search*
      - upload-gitlab-release*
      - warning*
  + git/
    - .gitconfig
  + input/
    - .inputrc
  + ssh/
  	- config
  + tmux/
    + .tmux/
      - reset
      - .tmux.conf.darwin
    - .tmux.conf
  + vim/
    + .vim/
      + autoload
        - pathogen.vim
      + colors
        - monokai-soda.vim
	  - .vimrc
- .dockerignore
- .gitignore
- .gitmodules
- Dockerfile
- README.md
```

### Bash-related files

| File/Directory  | Description                                                  |
| --------------- | ------------------------------------------------------------ |
| .bash_profile   | Main file to source in bash (>= 4.0)                         |
| .bash_aliases   | Common aliases for Linux and macOS                           |
| .bash_functions | Common functions like source_if, prename, rgbtohex           |
| .bash_prompt    | Builds the prompt supporting virtualenvs and git             |
| .profiles       | Contains a collection of files that are sourced for their respective tools |

### bin-related files

| File                     | Description                                                  |
| ------------------------ | ------------------------------------------------------------ |
| .bin/{msg,warning,error} | Echo text to stdout in different colors (blue, yellow, red)  |
| .bin/extract             | Generic unzip/unarchive/extract that performs based on file type |
| .bin/search              | Performs a find on files but filters `Permission denied` messages |
| .bin/safe_rm             | Used in the `rm` alias to move objects to Trash instead of to oblivion |

> **INFO:** These files are automatically sourced by the .bash_profile and can be used from anywhere

### vim-related files

| File   | Description                                                  |
| ------ | ------------------------------------------------------------ |
| .vimrc | Contains mainly convenience settings for searching, highlighting and which plugins are used |
| .vim   | Contains `Vundle` for plugin managment as well as the `Monokai-soda` theme |

### tmux-related files
| File   | Description                                                  |
| ------ | ------------------------------------------------------------ |
| .tmux.conf | Contains mainly convenience settings for hotkeys, look n feel and which plugins are used |
| .tmux   | Contains `tpm` for plugin managment |

### input-related files

### git-related files

### ssh-related files

## 2. Requirements

| Tool | Version |
| ---- | ------- |
| Bash | >= 4.4  |
| tmux | >= 2.6  |
| vim  |         |

## 3. Usage

> **TBD**
