set nocompatible

" Use pathogen to easily modify the runtime path to include all plugins under
" the ~/.vim/bundle directory
filetype off                    " force reloading *after* pathogen loaded

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle
" required!
Plugin 'VundleVim/Vundle.vim'

" My Bundles here
"
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'Valloric/YouCompleteMe'
Plugin 'ollykel/v-vim'
call vundle#end()

" Vundle plugin settings
" YouCompleteMe
let g:ycm_autoclose_preview_window_after_completion=1


filetype plugin indent on       " enable detection, plugins and indenting in one step
syntax on

" change the mapleader from \ to ,
let mapleader=","

" Quickly edit/reload the vimrc file
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>
nmap <silent> <leader>cv :bd<CR>

nnoremap j gj
nnoremap k gk
nnoremap <leader>s :mksession!<CR>

nnoremap <Space> i_<Esc>r

set hidden        " hide buffers
set nowrap        " don't wrap lines
set expandtab     " tab to spaces
set tabstop=2     " a tab is four spaces
set softtabstop=2 " when hitting <BS>, pretend like a tab is removed, even if spaces
set backspace=indent,eol,start
                    " allow backspacing over everything in insert mode
set autoindent    " always set autoindenting on
set copyindent    " copy the previous indentation on autoindenting
set number        " always show line numbers
set shiftwidth=2  " number of spaces to use for autoindenting
set shiftround    " use multiple of shiftwidth when indenting with '<' and '>'
set showmatch     " set show matching parenthesis
set ignorecase    " ignore case when searching
set smartcase     " ignore case if search pattern is all lowercase,
                    "    case-sensitive otherwise
set smarttab      " insert tabs on the start of a line according to
                    "    shiftwidth, not tabstop
set hlsearch      " highlight search terms
set incsearch     " show search matches as you type

set history=1000         " remember more commands and search history
set undolevels=1000      " use many muchos levels of undo
set wildignore=*.swp,*.bak,*.pyc,*.class
set title                " change the terminal's title
set visualbell           " don't beep
set noerrorbells         " don't beep

set nobackup
set noswapfile

set autoread

if has('autocmd')
	autocmd FileType python set expandtab
	autocmd FileType yaml setlocal ai ts=2 sts=2 sw=2 expandtab
	autocmd FileType vlang setlocal ai ts=2 sts=2 sw=2 expandtab
endif

if &t_Co >= 256 || has("gui_running")
    colorscheme monokai-soda
endif

if &t_Co > 2 || has("gui_running")
    " switch syntax highlighting on, when the terminal has colors
    syntax on
endif

set list
set listchars=tab:>.,trail:.,extends:#,nbsp:.

if has('autocmd')
	autocmd FileType html,xml set listchars-=tab:>.
"	autocmd filetype php colorscheme juicy
endif

command! W :execute ':silent w !sudo tee % > /dev/null' | :edit!
command! P :execute ':set paste! paste?'

let g:netrw_liststyle = 3
let g:netrw_banner = 0
let g:netrw_browse_split = 4
let g:netrw_winsize = 25
let g:netrw_altv = 1

nnoremap <silent> + :exe "vertical resize " . (winwidth(0) * 3/2)<CR>
nnoremap <silent> - :exe "vertical resize " . (winwidth(0) * 2/3)<CR>
nnoremap <silent> = <C-w>=<CR>.
command! Same :execute ':let g:netrw_browse_split = 0'
command! Other :execute ':let g:netrw_browse_split = 4'

if &diff
    set cursorline
    map n ]c
    map p [c
    hi DiffText   ctermfg=233  ctermbg=yellow  guifg=#000033 guibg=#DDDDFF gui=none cterm=none
endif
