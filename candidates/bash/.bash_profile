#!/usr/bin/env bash

shopt -s extglob

# add own binary helpers to path
export PATH="$HOME/.bin:$PATH"

# add custom bash functions
source ~/.bash_functions

# source .bashrc if this is the one that's loaded first
# source_if ~/.bashrc

# add bash aliases
source_if ~/.bash_aliases

if [[ "$OSTYPE" == "darwin"* || "$OSTYPE" == "linux"* ]]; then
  export LC_ALL="en_US.UTF-8"
  export LANG="en_US.UTF-8"
  export LANGUAGE="en_US.UTF-8"
fi

source_if ~/.bash_history_config

init

###############################################################################
###                               ALIASES                                   ###
###############################################################################


if [[ -x "${HOME}/.bin/safe_rm" ]]; then
  alias rm="${HOME}/.bin/safe_rm"
fi
if type reminder_cd >/dev/null 2>&1; then
  alias cd=reminder_cd
fi
alias_if 'unbuffer '

if [[ $OSTYPE == darwin* ]]; then
  if type brew >/dev/null 2>&1; then

    export HOMEBREW_CASK_OPTS="--no-quarantine"

    if [[ "${BASH_VERSINFO[0]}" -ge 4 ]]; then
      if [[ "${BASH_VERSINFO[0]}" -ge 5 ]]; then
        export BASH_COMPLETION_COMPAT_DIR="$(brew --prefix)/etc/bash_completion.d"
        source_if "$(brew --prefix)/etc/profile.d/bash_completion.sh"
      else
        source_if "$(brew --prefix)/share/bash-completion/bash_completion"
      fi
      if type git >/dev/null 2>&1; then
        source_if "$(brew --prefix)/etc/bash_completion.d/git-completion.bash"
      fi
      if type git-flow >/dev/null 2>&1; then
        source_if "$(brew --prefix)/etc/bash_completion.d/git-flow-completion.bash"
      fi
    fi
  fi
elif [[ $OSTYPE == linux* ]]; then
  source_if /usr/share/bash-completion/bash_completion
  if type git-flow >/dev/null 2>&1; then
    source_if /etc/bash_completion.d/git-flow
  fi
fi

source_if ~/.bash_prompt

export PATH="/usr/local/bin:$PATH"
export PATH="/usr/local/sbin:$PATH"
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"

###############################################################################
###                          PACKAGE PROFILES                               ###
###############################################################################
if [[ -d $HOME/.profiles ]]; then
  for f in "$HOME"/.profiles/.[!.]*; do
    source "$f"
  done
fi

# add own binary helpers to path
export PATH="$HOME/.bin:$PATH"