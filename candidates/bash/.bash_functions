#!/bin/usr/env bash

function basedir() {
  local source="${BASH_SOURCE[1]}"
  local basedir
  while [ -h "$source" ]; do # resolve $SOURCE until the file is no longer a symlink
    basedir="$( cd -P "$( dirname "$source" )" >/dev/null 2>&1 && pwd )"
    source="$(readlink "$source")"
    [[ $SOURCE != /* ]] && SOURCE="$basedir/$source" # if $source was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  done
  basedir="$( cd -P "$( dirname "$source" )" >/dev/null 2>&1 && pwd )"
  echo $basedir
}

declare BASEDIR=$(basedir)
declare BINDIR="$BASEDIR/../bin/.bin"

function source_if() {
  if [[ -f $1 ]]; then
    . "$1"
  else
    error "[$1] does not exist"
  fi
}

function alias_if() {
  local cmd
  cmd=$(echo "$1" | cut -d' ' -f1)
  if type "$cmd" >/dev/null 2>&1; then
    alias "$cmd"="$1"
  else
    error "[$cmd] is missing"
  fi
}

function init() {
  if [[ (-f ~/.hushlogin) && (-f /etc/motd) ]]; then
    "$BINDIR"/lean "$(cat /etc/motd)"
  fi
}

function reminder_cd() {
  builtin cd "$@" && ({
    if [ -f .cd-reminder ]; then
      "$BINDIR"/lean "$(cat .cd-reminder)"
    fi
  })
}

rgbtohex () {
  # usage) `rgbtohex 17 0 26` ==> 1001A
  # usage) `rgbtohex -h 17 0 26` ==> #1001A
  addleadingzero () { awk '{if(length($0)<2){printf "0";} print $0;}';}
  if [[ ${1} == "-h" ]]; then
    r=${2}; g=${3}; b=${4};h='#';
  else
    r=${1}; g=${2}; b=${3};h='';
  fi
  r=$(echo "obase=16; ${r}" | bc | addleadingzero)
  g=$(echo "obase=16; ${g}" | bc | addleadingzero)
  b=$(echo "obase=16; ${b}" | bc | addleadingzero)
  echo "${h}${r}${g}${b}"
}

rgbto256 () {
  # usage: `rgbto256 0 95, 135` ==> 22
  echo "define trunc(x){auto os;os=scale;scale=0;x/=1;scale=os;return x;};" \
    "16 + 36 * trunc(${1}/51) + 6 * trunc(${2}/51) +" \
    " trunc(${3}/51)" | bc
  # XTerm Color Number = 16 + 36 * R + 6 * G + B | 0 <= R,G,B <= 5
}

hextorgb () {
  # usage) `hexttorgb "11001A" ==> 17 0 26
  # usage) `hexttorgb "#11001A" ==> 17 0 26
  hexinput=$(echo "${1}" | tr '[:lower:]' '[:upper:]')  # uppercase-ing
  hexinput=$(echo "${hexinput}" | tr -d '#')          # remove Hash if needed
  a=$(echo "${hexinput}" | cut -c-2)
  b=$(echo "${hexinput}" | cut -c3-4)
  c=$(echo "${hexinput}" | cut -c5-6)
  r=$(echo "ibase=16; ${a}" | bc)
  g=$(echo "ibase=16; ${b}" | bc)
  b=$(echo "ibase=16; ${c}" | bc)
  echo "${r}" "${g}" "${b}"
}
