#!/usr/bin/env bash

alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

alias grep='grep --color=auto'

alias ll='ls -lahF'
alias la='ls -lhF'

psf() {
  ps -eo pid,state,user,command | grep -v grep | grep "$1"
}

# find all filteypes in directory structure
types() {
  find "$1" -type f | perl -ne 'print "$1" if m/\.([^.\/]+)$/' | sort -u
}

if type sudo >/dev/null 2>&1; then
  alias sudo='sudo '
fi

if type tree >/dev/null 2>&1; then
  alias tree='tree -FC --dirsfirst'
fi

if [[ "$OSTYPE" == "darwin"* ]]; then
  # Do something under Mac OS X platform
  alias ll='ls -laheFOG'
  alias la='ls -lheFOG'

  if type tree >/dev/null 2>&1; then
    alias tree='tree -FC --dirsfirst'
  fi
elif [[ "$OSTYPE" == "linux"* ]]; then
  # some more ls aliases
  alias ls='ls --color=auto'
  alias ll='ls -lahF --group-directories-first'
  alias la='ls -A'

  if type xclip >/dev/null 2>&1; then
    alias pbcopy='xclip -selection clipboard'
    alias pbpaste='xclip -selection clipboard -o'
  fi
fi
